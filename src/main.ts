import Vue from 'vue'
import App from './App.vue'
import router from './.router.js'
import '@/css/reset.css'
import 'nprogress/nprogress.css';
import VueCompositionApi from '@vue/composition-api';

Vue.use(VueCompositionApi);

Vue.config.productionTip = false;

/*
 * 公共部分去除第三方库的引入，在需要的模块下单独引入。
 * 公共部分应尽量保持最小化。
 */
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  render: h => h(App)
})