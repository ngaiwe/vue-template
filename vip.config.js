module.exports = {
  package: {
    assetsPublicPath: '/{{assetsPublicPath}}/', // 打包后的目录地址
    onlineAssetsPublicPath: '//s.vipkidstatic.com/beeschool/{{assetsPublicPath}}/', // 正式环境打包后的目录地址
    assetsPublicPathDll: '/{{assetsPublicPath}}/', // dll打包地址
    autoTutor: '/{{assetsPublicPath}}/', // 自动化路由
    openPage: '{{assetsPublicPath}}', // 自动打开页面
  }
}