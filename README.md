## 项目规范

#### 一.项目命令

- yarn setup 安装企业私仓库
- yarn dll 提取vue等大型稳定版本项目
- yarn dev 启动devserver
- yarn build:test 测试环境打包
- yarn build:online 线上环境打包
- yarn build:report 查看打包chunk大小便于优化

#### 二.项目结构 /src
- **api**
  
  项目接口层抽离处理 业务层面抽离 如有重复业务接口多页面调用不进行单独抽离，只针对于业务层面抽离和处理数据
  
- **assets**
  
  项目静态文件 包含image icon font(图标字体)
  
- **components**
  
  项目公共组件 结构中不含有common文件夹和业务层组件 按照模块划分组件文件夹
  

- **containers**
  
  项目业务**公共组件**和容器组件 命名请以Logic开头
  
- **css**
  
  项目css文件 公共处理css文件和相应模块化less存储

  
- **page**
  
  项目页面 按照业务划分一个文件中只含有一个页面文件,可以含有当前业务组件 
  
  
- **util**
  
  项目工具类 